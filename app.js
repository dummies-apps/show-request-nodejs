const express = require("express"),
  app = express();

app.get("/*", function (req, res) {
  var fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  res.send(`${fullUrl}`);
});

app.listen(8000, function () {
  console.log(`show-request-nodejs is listening on port 8000 !!!`);
});
