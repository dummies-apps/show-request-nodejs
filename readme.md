# Show request node app

## Description

This is a simple node js application that we will use to make examples in other projects like Kubernetes.

The code is defined in the `app.js` file. We've declared an interceptor to intercept all requests and display the requested route.

## Installation

If you need to get the app without installation, you can get it from docker hub :

    $ docker pull mouhamedali/show-request-nodejs

### Local machine

On the current directory, run this command to build the docker image :

    $ docker build -t show-request-nodejs .

Run the app :

    $ docker run -p 8080:8000 show-request-nodejs

Test the app :

    $ curl localhost:8080

```log
http://localhost:8080/
```

    $ curl localhost:8080/some/dummy/route

```log
http://localhost:8080/some/dummy/route
```

    $ curl localhost:8080/index.html?locale=en

```log
http://localhost:8080/index.html?locale=en
```

### Push it on DockerHub

    $ docker tag show-request-nodejs <your-docker-id>/show-request-nodejs

in my case :

    $ docker tag show-request-nodejs mouhamedali/show-request-nodejs

push it :

    $ docker push mouhamedali/show-request-nodejs

### Push it on GCR (Google Container Registry)

On the current directory, run this command to build the docker image :

    $ docker tag  show-request-nodejs eu.gcr.io/[YOUR-PROJECT-ID]/show-request-nodejs

To get the list of your projects ids, run this command :

    $ gcloud projects list

Now, run this command to push the image (you have to log in before) :

    $ gcloud docker -- push eu.gcr.io/[YOUR-PROJECT-ID]/show-request-nodejs

You can check this url to see if the images has been pushed correctly :

- [google container registry](https://console.cloud.google.com/gcr)
